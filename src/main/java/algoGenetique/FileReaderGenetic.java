package algoGenetique;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

public class FileReaderGenetic {

	public static void readData(List<ItemGenetic> items, List<BoxGenetic> boxes, String fileName) throws FileNotFoundException {
		File file = new File("./src/main/resources/benchs/"+fileName);

		Scanner s = new Scanner(file);

		int value = Integer.parseInt(s.next());

		for (int i = 0; i < value; i++) {
			BoxGenetic box = new BoxGenetic();
			int capacity = Integer.parseInt(s.next());
			box.setCapacity(capacity);
			box.setLeftCapacity(capacity);
			boxes.add(box);
		}

		Integer.parseInt(s.next());
		Integer.parseInt(s.next());

		int id = 0;
		while (s.hasNext()) {
			ItemGenetic item = new ItemGenetic();
			item.setWeight(Integer.parseInt(s.next()));
			item.setColor(Integer.parseInt(s.next()));
			item.setId(id);
			items.add(item);
			id++;
		}
		s.close();
	}
}
