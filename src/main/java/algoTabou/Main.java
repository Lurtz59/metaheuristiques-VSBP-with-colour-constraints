package algoTabou;

import algoTabou.Box;
import algoTabou.FileReader;
import algoTabou.Item;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		List<Box> boxes = new ArrayList<Box>();
		List<Item> items = new ArrayList<Item>();
		
		try {
			FileReader.readData(items, boxes, "bench_2_0");
			
			for (Item item : items) {
				System.out.println(item.toString());
			}
			for (Box box : boxes) {
				System.out.println(box.toString());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
